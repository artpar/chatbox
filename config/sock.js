var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var client = require('./connection');
var api = require('./api');
var StateMachine = require('javascript-state-machine');


client.on("error", function (err) {
  console.log("Error " + err);
});

function titleCase(s) {
  s = s.replace(/([_\-]\w)/g, function (m) {
    return m[1].toUpperCase();
  });
  // s = s[0].toUpperCase() + s.substring(1);
  return s;
}


//socket.io connect
var conn = function () {
  server.listen(8880);
  app.get('/', function (req, res) {
    res.sendfile(__dirname + '/index.html');
  });
};

function keyName() {
  var args = Array.prototype.slice.call(arguments, keyName.length);
  return args.join(',');
}

//return from server and pass to botui
var fromClient = function () {
  io.on('connection', function (socket) {
    socket.on('fromClient', function (data) {
      console.log("Client", data.sessionId, " says ", data.client);


      client.get(data.sessionId, function (err, response) {
        if (!response) {
          response = "unknown_query"
        }
        console.log("Current conversation state", response);


        const machine = new StateMachine({
          init: response,
          transitions: [
            {name: 'noElectricity', from: '*', to: 'DetailsForNoElectricity'},
            {name: 'dontHaveConnectionNumber', from: 'DetailsForNoElectricity', to: 'GetConnectionNumberForNoElectricity'},
            {name: 'haveConnectionNumber', from: 'DetailsForNoElectricity', to: 'InitiateBillPay'},
            {name: 'mentionedConnectionNumber', from: 'GetConnectionNumberForNoElectricity', to: 'InitiateBillPay'},
            {name: 'billPaid', from: 'InitiateBillPay', to: 'QueryResolved'},
            {name: 'billAlreadyPaid', from: '*', to: 'DetailsforBillalreadyPaid'},
            {name: 'powerBackup', from: '*', to: 'DetailsForPowerBackup'},
            {name: 'checkPowerBackupPromise', from: 'powerBackup', to: 'PowerbackUpPromise'},
            {name: 'billAmountHigh', from: '*', to: 'DetailsforBillAmountHigh'},
            {name: 'chatwithcareAgent', from: 'billAmountHigh', to: 'CheckBillAmount'},
            {name: 'prevTenentsDidntPay', from: '*', to: 'DetailsforArrears'},
            {name: 'billSplittingLogic', from: '*', to: 'DetailForBillSpliting'}
          ],
          methods: {
            onNoElectricity: function (fsm, parameters) {
              console.log("No electricity Intent detected", parameters);
              return new Promise(function (resolve, reject) {
                console.log('No Electricity Intent detected, checking for electricity details..', parameters.sessionId);
                client.get(keyName(parameters.sessionId, "connection_number"), function (err, connectionNumber) {
                  if (!connectionNumber) {
                    resolve({
                      nextTrigger: "dontHaveConnectionNumber"
                    })
                  } else {
                    resolve({
                      nextTrigger: "haveConnectionNumber"
                    })
                  }
                });

              })
            },
            onDontHaveConnectionNumber: function () {
              console.log('ask user if they have the details - if not ask them to get it from security');
              return new Promise(function (resolve, reject) {
                resolve({
                  response: "Okay, can you help us out with your connection number?",
                })
              })
            },
            onMentionedConnectionNumber: function (fsm, parameters) {
              console.log("User provided connection number", parameters);
              return new Promise(function (resolve, reject) {
                client.set(keyName(parameters.sessionId, "connection_number"), parameters.connection_number)
                resolve({
                  response: "Ok..",
                  nextTrigger: "billPaid"
                })
              })
            },
            onHaveConnectionNumber: function () {
              console.log('Great! User has connection number');
              return new Promise(function (resolve, reject) {
                resolve({
                  response: "Ok, we have your connection details. We will check and pay the pendind dues now.",
                  nextTrigger: "billPaid"
                })
              })
            },
            onBillPaid: function () {
              console.log('Bill paid with Billdesk - offline settlement later');
              return new Promise(function (resolve, reject) {
                resolve({
                  response: "We have paid the pending dues. Hope that helps..",
                })
              })
            },
            onBillAlreadyPaid: function () {
              console.log('User seems to have paid the bill - asking to update dashboard');
              return new Promise(function (resolve, reject) {
                resolve({
                  response: "Ok, we will update the dashboard. Please check it in a while..",
                })
              })
            },
            onPowerBackup: function () {
              console.log('User needs power back up..');
              return new Promise(function (resolve, reject) {
                resolve({
                  response: "Hey, we will check with the property manager if power back up was promised and get back to you..",
                  nextTrigger: "checkPowerBackupPromise"
                })
              })
            },
            onCheckPowerBackupPromise: function () {
              console.log('Checking with area manager if power back up was promised..');
              return new Promise(function (resolve, reject) {
                resolve({
                  response: "Hey, we checked with our property manager and he says the inverter was to be installed at your expenses..",
                })
              })
            },
            onBillAmountHigh: function () {
               return new Promise(function (resolve, reject) {
                    resolve({
                        response: "Hmm, pending dues for your house are Rs 13,123. Let me connect you with our care agent.",
                        nextTrigger: "chatwithcareAgent"
                    })
               })
            },
            onChatWithCareAgent: function () {
                return new Promise(function (resolve, reject) {
                                    resolve({
                                        response: "Hey, Shardul here - how can I help you?",
                                    })
                               })
            },
            onPrevTenentsDidntPay: function(){
                return new Promise(function (resolve, reject) {
                                    resolve({
                                        response: "Hey, we see that previous tenants haven't paid - Nestaway will bear the charges and pay the dues..",
                                        nextTrigger: "billPaid"
                                    })
                               })
            },
            onBillSplittingLogic: function() {
                return new Promise(function (resolve, reject) {
                                                    resolve({
                                                        response: "We check move in dates of each tenants, calculate their share in each month's electricity bills and split them per their share. \n E.g. if Sam moved in at 15th of May - he will be charged only for 15 days of May. \n Hope this helps."
                                                    })
                                               })
            }
          }
        });


        api.getRes(data.client, data.sessionId).then(function (res) {
          let triggerName = titleCase(res.result.metadata.intentName);
          console.log("got result from api.getRes", triggerName, " on ", machine.state, res.result.parameters);

          const triggerHandle = function (result) {
            console.log("State change completed to new state", machine.state);
            client.set(data.sessionId, machine.state, client.print);

            if (result.response) {
              console.log("state response", result.response)
              socket.emit('fromServer', {server: result.response});
            }


            if (result.nextTrigger && machine[result.nextTrigger]) {
              machine[result.nextTrigger]({
                sessionId: data.sessionId
              }).then(triggerHandle);
            }

          };

          try {
            if (machine[triggerName]) {
              const parameters = res.result.parameters;
              parameters.sessionId = data.sessionId;
              console.log("trigger with params", parameters);
              machine[triggerName](parameters).then(triggerHandle);
            } else {
              socket.emit('fromServer', {server: res.result.fulfillment.speech});
            }
          } catch (e) {
            console.log("failed to trigger ", e);
          }
        });

      });

    });//end socket.on
  });
};//end function


module.exports = {conn, fromClient};
