var apiai = require('apiai');



//API Key
var app = apiai("5feaeb72ff42444d9976e2d7f878acfc");

// Function which returns speech from api.ai
var getRes = function(query, sessionId) {
  var request = app.textRequest(query, {
      sessionId: sessionId
  });
  const responseFromAPI = new Promise(function (resolve, reject) {
    request.on('error', function(error) {
      reject(error);
    });
    request.on('response', function(response) {
      console.log(response)
      resolve(response);
    });
  });
  request.end();
  return responseFromAPI;
};

// test the command :

module.exports = {getRes}
