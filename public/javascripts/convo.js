var botui = new BotUI('api-bot');

var socket = io.connect(window.location.hostname + ":8880");

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}


const sessionId = uuidv4();

socket.removeAllListeners();
botui.message.add({
  content: 'Lets Start Talking...'
}).then(() => {
  init()
});

socket.on('fromServer', function (data) { // recieveing a reply from server.
  console.log(data);
  botui.message.add({
    content: data.server,
    delay: 500,
  });
});

function init() {
  botui.action.text({
    action: {
      placeholder: 'Say something...'
    }
  }).then(function (res) {
    socket.emit('fromClient', {client: res.value, sessionId: sessionId}); // sends the message typed to server
    console.log(res);
  }).then(init);
};
init();
